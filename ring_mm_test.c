#include <stdio.h>
#include "ring_mm.h"



int main( int argc, char **argv ) { 

    struct pfring_mm_state *state;
    struct ring_mm_data_header *header;
    unsigned int packet_count = 0;
    int read_thru = 0;
    int index;

    printf("starting\n");
    fflush(stdout);

    state = pfring_mm_open("eth1", (1024 * 1024 * 1), 1, 1600);
    header = state->ring_memory;

    printf("got back a state for %s, header at %p\n", state->name, header);
    fflush(stdout);

    for ( index=0; index < 10; index++ ) {
	sleep(10);
	read_thru = header->write_offset;
	packet_count = pfring_mm_read(state, read_thru);

	printf("header packet_count=%d, read=%u, write=%u, "
	       "captured=%u/%llu, dropped=%u/%llu\n", packet_count,
	       header->read_offset, header->write_offset,
	       header->capture_count, header->total_capture_count,
	       header->drop_count, header->total_drop_count);
	fflush(stdout);
    }

    printf("sleeping before final read\n");
    fflush(stdout);
    sleep(120);

    packet_count = pfring_mm_read(state, read_thru);
    printf("header packet_count=%d, read=%u, write=%u, "
	   "captured=%u/%llu, dropped=%u/%llu\n", packet_count,
	   header->read_offset, header->write_offset,
	   header->capture_count, header->total_capture_count,
	   header->drop_count, header->total_drop_count);
    fflush(stdout);

    printf("exiting\n");
    fflush(stdout);
}
