/*
 * Copyright (C) 2007, NEXVU Technologies
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/sockios.h>

#include "ring_mm.h"

#if !defined(MIN) 
# define MIN(x,y)	((x) <= (y) ? (x) : (y) )
#endif


/*****************************************
 * pfring_mm_open()
 */
struct pfring_mm_state *pfring_mm_open(char *name, int size,
				       int promisc, int snaplen) {

    void *memory;
    struct pfring_mm_state *state;
    struct sockaddr sa;
    struct ring_mm_req packet_rx_ring_req;
    int retval;

    /* Allocate the ring buffer first -- largest, is most likely to fail. */
    memory = malloc(size);
    if ( memory == NULL ) {
	fprintf(stderr, "pfring_mm_open - failed to allocate ring buffer: "
		"%d (%s)\n", errno, strerror(errno) );
	return NULL;
    }
    memset(memory, 0, size);

    /* Allocate memory for the userland state object. */
    state = (struct pfring_mm_state *) malloc(sizeof(struct pfring_mm_state));

    if ( state == NULL ) {
	fprintf(stderr, "pfring_mm_open - failed to allocate state "
		"memory: %d (%s)\n", errno, strerror(errno) );
	free(memory);
	return NULL;
    }

    /* initialize the state */
    memset(state, 0, sizeof(struct pfring_mm_state));
    snprintf(state->name, sizeof(state->name)-1, "%s", name);
    state->ring_memory = memory;
    state->ring_size = size;

    /* get the socket */
    state->sock_fd = socket(PF_RING, SOCK_RAW, htons(ETH_P_ALL));
    if (state->sock_fd <= 0) {
	fprintf( stderr, "pfring_mm_open - failed to open socket: %d (%s)\n",
		 errno, strerror(errno) );
	free(state);
	free(memory);
	return NULL;
    }

    sa.sa_family = PF_RING;
    snprintf(sa.sa_data, sizeof(sa.sa_data)-1, "%s", name);
    
    retval = bind( state->sock_fd, &sa, sizeof(sa) );
    if ( retval < 0 ) {
	fprintf( stderr, "pfring_mm_open - failed to bind socket: %d (%s)\n",
		 errno, strerror(errno) );
	close(state->sock_fd);
	free(state);
	free(memory);
	return NULL;
    }

    retval = setsockopt(state->sock_fd, 0, RING_MM_SNAPLEN, NULL, snaplen );
    if ( retval < 0 ) {
	fprintf( stderr, "pfring_mm_open - failed to set snaplen: "
		 "%d (%s)\n", errno, strerror(errno) );
	close(state->sock_fd);
	free(state);
	free(memory);
	return NULL;
    }

    packet_rx_ring_req.memory_address = (unsigned long) state->ring_memory;
    packet_rx_ring_req.memory_size    = state->ring_size;
    retval = setsockopt(state->sock_fd, 0, PACKET_RX_RING, &packet_rx_ring_req,
			sizeof(struct ring_mm_req) );
    if ( retval <= 0 ) {
	fprintf( stderr, "pfring_mm_open - failed to set ring buffer: "
		 "%d (%s)\n", errno, strerror(errno) );
	close(state->sock_fd);
	free(state);
	free(memory);
	return NULL;
    }

    if ( promisc ) {
	pfring_mm_set_promisc(state, TRUE);
    }

    return state;
}


/*****************************************
 * pfring_mm_close()
 */
void pfring_mm_close(struct pfring_mm_state *state) {

    if( !state ) {
	fprintf(stderr, "pfring_mm_close - null state\n");
	return;
    }

    if(state->clear_promisc)
	pfring_mm_set_promisc(state, FALSE);

    close(state->sock_fd);
    free(state->ring_memory);
    free(state);
}



/*****************************************
 * pfring_mm_set_promisc()
 */
int pfring_mm_set_promisc(struct pfring_mm_state *state, int promisc) {

    struct ifreq ifrequest;
    int retval;

    if ( state == NULL ) {
	fprintf(stderr, "pfring_mm_set_promisc - null state\n");
	return -EINVAL;
    }

    memset(&ifrequest, 0, sizeof(struct ifreq));
    strncpy(ifrequest.ifr_name, state->name,
	    MIN( (int)sizeof(ifrequest.ifr_name), PFRING_MM_NAME_SIZE) );

    retval = ioctl(state->sock_fd, SIOCGIFFLAGS, &ifrequest);
    if ( retval < 0 ) {
	fprintf(stderr, "pfring_mm_set_promisc - unable to get "
		"interface flags.\n");
	return retval;
    }

    if ( promisc ) {
	ifrequest.ifr_flags |= IFF_PROMISC;
	state->clear_promisc = TRUE;
    } else {
	ifrequest.ifr_flags &= ~IFF_PROMISC;
	state->clear_promisc = FALSE;
    }

    retval = ioctl(state->sock_fd, SIOCSIFFLAGS, &ifrequest);
    if ( retval < 0 ) {
	fprintf(stderr, "pfring_mm_set_promisc - unable to set "
		"interface flags.\n");
    }

    return retval;
}



/*****************************************
 * pfring_mm_read()
 */
int pfring_mm_read(struct pfring_mm_state *state, int read_offset) {

    int retval;

    if ( state == NULL ) {
	fprintf(stderr, "pfring_mm_read - null state\n");
	return -EINVAL;
    }

    retval = setsockopt(state->sock_fd, 0, RING_MM_FLIP, NULL, read_offset );
    if ( retval < 0 ) {
	fprintf( stderr, "pfring_mm_read - read call failed: "
		 "%d (%s)\n", errno, strerror(errno) );
    }

    return retval;
}



/*****************************************
 * pfring_mm_get_ring_address()
 */
void *pfring_mm_get_ring_address(struct pfring_mm_state *state) {

    int retval;

    if ( state == NULL ) {
	fprintf(stderr, "pfring_mm_getring - null state\n");
	return (void *)-EINVAL;
    }

    return (void *) (((unsigned long) state->ring_memory) +
		     sizeof(struct ring_mm_data_header));
}



/*****************************************
 * pfring_mm_get_data_capacity()
 */
int pfring_mm_get_data_capacity(struct pfring_mm_state *state) {
    return state->ring_size - sizeof(struct ring_mm_data_header);
}



/*****************************************
 * pfring_mm_get_read_offset()
 */
int pfring_mm_get_read_offset(struct pfring_mm_state *state) {
    return ((struct ring_mm_data_header *)state->ring_memory)->read_offset;
}



/*****************************************
 * pfring_mm_get_write_offset()
 */
int pfring_mm_get_write_offset(struct pfring_mm_state *state) {
    return ((struct ring_mm_data_header *)state->ring_memory)->write_offset;
}



/*****************************************
 * pfring_mm_get_wrap_offset()
 */
int pfring_mm_get_wrap_offset(struct pfring_mm_state *state) {
    return ((struct ring_mm_data_header *)state->ring_memory)->wrap_offset;
}



/*****************************************
 * pfring_mm_get_drop_count()
 */
int pfring_mm_get_drop_count(struct pfring_mm_state *state) {
    return ((struct ring_mm_data_header *)state->ring_memory)->drop_count;
}


