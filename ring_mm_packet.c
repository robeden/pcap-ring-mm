/*
 * Copyright (C) 2007, NEXVU Technologies
 *            ring_mm_packet.c, based in large part on
 *            Luca Deri's ring_packet.c (C) 2004-2007
 *
 * This code includes code by
 * - Jeff Randall <jrandall@nexvu.com>
 * - Luca Deri <deri@ntop.org>
 * - Helmut Manck <helmut.manck@secunet.com>
 * - Brad Doctor <brad@stillsecure.com>
 * - Amit D. Chaudhary <amit_ml@rajgad.com>
 * - Francesco Fusco <fusco@ntop.org>
 * - Michael Stiller <ms@2scale.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */


/*
 * Design goals:
 *
 * - Put the packets directly into a buffer mmapped from user-space.
 *
 * - There is a header at the start of the buffer that contains the offset of
 *   the first byte to be read and the offset to where the next byte will be
 *   written when the next packet is captured.  The offsets will be measured
 *   from the start of the memory available to store packets (first byte 
 *   after the header itself) and will have valid values from 0 to
 *   memory_size - sizeof(header) - 1
 *
 * - Following the buffer header, each packet captured is placed into the
 *   buffer with the pacp_header information first, then the captured packet
 *   itself.  Subsequent packets captured will be placed directly after the
 *   preceeding packet.  eg:
 *
 *   ---------------------------------------------------------------------
 *   |       next_read_offset          |       next_write_offset         |
 *   ---------------------------------------------------------------------
 *   |         wrap_pointer            |            reserved             |
 *   ---------------------------------------------------------------------
 *   |         capture_count           |           drop_count            |
 *   ---------------------------------------------------------------------
 *   |                        total_capture_count                        |
 *   ---------------------------------------------------------------------
 *   |                          total_drop_count                         |
 *   ---------------------------------------------------------------------
 *   | pcap_header #1 | packet_data #1 | pcap_header #2 | packet_data #2 |
 *   ---------------------------------------------------------------------
 *   | ...                             | pcap_header #n | packet_data #n |
 *   ---------------------------------------------------------------------
 *
 *   NOTE: sizeof(struct pcap_pkthdr) should not be used here as it reports
 *         the 'kernel' size of 40 bytes on i386 and not the intended 16 bytes.
 *         that userland sees.
 *
 * - A circular-buffer system will be used.
 *
 *   This setup allows for the most efficient use of available memory
 *   possible with the only wasted space being that left at the end of
 *   a buffer lacking room to hold an additional packet.
 *
 *   NOTE: I am not intending to support kernels older than 2.6.0
 *
 */


#include <linux/version.h>
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19))
#include <linux/autoconf.h>
#else
#include <linux/config.h>
#endif
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/socket.h>
#include <linux/skbuff.h>
#include <linux/rtnetlink.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <linux/in6.h>
#include <linux/init.h>
#include <linux/filter.h>
#include <linux/ring.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/list.h>
#include <linux/proc_fs.h>
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
#include <net/xfrm.h>
#else
#include <linux/poll.h>
#endif
#include <net/sock.h>
#include <asm/io.h>   /* needed for virt_to_phys() */
#ifdef CONFIG_INET
#include <net/inet_common.h>
#endif
#include "ring_mm.h"

#if 0
# define RING_MM_DEBUG
# undef  KERN_ERR
# define KERN_ERR
# undef  KERN_INFO
# define KERN_INFO
# undef  KERN_DEBUG
# define KERN_DEBUG
# if 0
#  define RING_MM_DEBUG_PACKETS
# endif
#endif

#if 0
# define RING_MM_SUPPORT_POLL
#endif


#define RING_MM_BUFFER_MIN_SIZE (1024 * 1024 * 1)
#define MAX_PACKET_VIS_SIZE	9000
#define MIN_SPACE_REMAINING	64

#if !defined(MIN)
# define	MIN(x,y)	((x) <= (y) ? (x) : (y))
#endif



/***************************************
 * Licensing and aliases.
 */
MODULE_LICENSE("GPL");
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0))
MODULE_ALIAS_NETPROTO(PF_RING);
#endif




/***************************************
 * Load time tunables
 */
#if defined(RING_MM_TRANSPARENT)
static unsigned int transparent_mode = 0;
module_param(transparent_mode, int, 0644);
MODULE_PARM_DESC(transparent_mode, "Non-zero enables transparent mode "
                 "(slower but backwards compatible)");
#endif

#if defined(RING_MM_TRANSMIT)
static unsigned int enable_tx_capture = 0;
module_param(enable_tx_capture, int, 0644);
MODULE_PARM_DESC(enable_tx_capture, "Non-zero captures outgoing packets");
#endif

#if defined(RING_MM_SAMPLING)
static unsigned int sample_rate = 1;
module_param(sample_rate, int, 0644);
MODULE_PARM_DESC(sample_rate, "Ring packet sample rate "
		 "(set to 1 for all packets)");
#endif






/***************************************
 * Structure definitions
 */
struct ring_mm_state {
    struct net_device	*netdev;

    unsigned int	 ring_id;
    unsigned int	 memory_size;
    unsigned int	 data_size;
    unsigned int	 pad_a;

    /* Data that will eventually be put in the data header on a 'read' */
    unsigned int	 read_offset;
    unsigned int	 write_offset;
    unsigned int	 wrap_offset;
    unsigned int	 pad_b;
    unsigned int	 capture_count;
    unsigned int	 drop_count;
    unsigned long long	 total_capture_count;
    unsigned long long	 total_drop_count;

    unsigned long	 mapped_address;
    unsigned long	 memory_address;
    unsigned long	 data_address;

    struct page		**raw_pages;
    unsigned int	 page_count;

    unsigned int	 snaplen;

#if defined(RING_MM_SAMPLING)
    unsigned int	 pkt_sample_rate;
    unsigned int	 pkt_sample_counter;
#endif

    struct sk_filter	*bpf_filter;

    spinlock_t		 lock;
    struct sk_buff	 skb;
};


struct ring_mm_element {
    struct list_head  list;
    struct rcu_head   rcu;
    struct sock      *sk;
};



/***************************************
 * Global variables
 */
/* /proc entry for ring module */
struct proc_dir_entry		*ring_mm_proc_dir = NULL;
struct proc_dir_entry		*ring_mm_proc     = NULL;

static int			 ring_count       = 0;
static struct list_head		 ring_mm_element_list;

static DEFINE_SPINLOCK(ring_mm_element_list_lock);



/***************************************
 * Prototypes
 */
static void ring_mm_proc_init(void);
static void ring_mm_proc_term(void);
static void ring_mm_proc_add(struct ring_mm_state *state);
static void ring_mm_proc_remove(struct ring_mm_state *state);
static int ring_mm_proc_get_info(char *, char **, off_t, int, int *, void *);

static int ring_mm_skb_handler(struct sk_buff *skb, unsigned char recv_packet,
			       unsigned char real_skb);

static int ring_mm_buffer_handler(struct net_device *dev, char *data, int len);

static int ring_mm_bind(struct socket *sock, struct sockaddr *sa,int addr_len);
static int ring_mm_release(struct socket *sock);

static int ring_mm_setsockopt(struct socket *sock, int level, int optname,
                              char *optval, int optlen);

static int ring_mm_ioctl(struct socket *sock, unsigned int cmd,
                         unsigned long arg);

static int ring_mm_recvmsg(struct kiocb *iocb, struct socket *sock,
                           struct msghdr *msg, size_t len, int flags);

static int ring_mm_create(struct socket *sock, int protocol);
static void ring_mm_destruct(struct sock *sk);


static int ring_mm_set_buffer(struct sock *sk,
                              struct ring_mm_req *request, int closing);


static inline void ring_mm_store_sk_entry(struct sock *sk);
static inline void ring_mm_remove_sk_entry(struct sock *sk);

static void add_skb_to_ring(struct sk_buff *skb, struct ring_mm_state *state,
                            unsigned char recv_pkt, unsigned char real_skb);


/*****************************************
 * The options structures to register at startup.
 */
static struct proto_ops ring_mm_ops = {
    .family       =       PF_RING,
    .owner        =       THIS_MODULE,

    /* Operations that make no sense on ring sockets. */
    .connect      =       sock_no_connect,
    .socketpair   =       sock_no_socketpair,
    .accept       =       sock_no_accept,
    .getname      =       sock_no_getname,
    .listen       =       sock_no_listen,
    .shutdown     =       sock_no_shutdown,
    .sendpage     =       sock_no_sendpage,
    .sendmsg      =       sock_no_sendmsg,
    .getsockopt   =       sock_no_getsockopt,

    /* Operations that we are interested in. */
    .release      =       ring_mm_release,
    .bind         =       ring_mm_bind,
    .setsockopt   =       ring_mm_setsockopt,
    .ioctl        =       ring_mm_ioctl,
    .recvmsg      =       ring_mm_recvmsg,

#if defined(RING_MM_SUPPORT_MMAP)
    .mmap         =       ring_mm_mmap,
#else
    .mmap	  =	  sock_no_mmap,
#endif

#if defined(RING_MM_SUPPORT_POLL)
    .poll         =       ring_mm_poll,
#else
    .poll         =       sock_no_poll,
#endif


};

static struct net_proto_family ring_mm_family_ops = {
    .family       =       PF_RING,
    .create       =       ring_mm_create,
    .owner        =       THIS_MODULE,
};

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,6,11))
static struct proto ring_mm_proto = {
    .name         =       "PF_RING",
    .owner        =       THIS_MODULE,
    .obj_size     =       sizeof(struct sock),
};
#endif

#if defined(RING_MM_SUPPORT_MMAP)
static struct vm_operations_struct ring_mm_mmap_ops = {
    .open  = ring_mm_mmap_open,
    .close = ring_mm_mmap_close,
};
#endif



/***************************************
 * ring_init()
 *
 * Called at driver load time, manages initial setup.
 */
static int __init ring_init(void)
{
    spin_lock_init(&ring_mm_element_list_lock);

    printk(KERN_INFO "PF_RING (memory mapped) version %s loading\n",
	   RING_MM_VERSION);

    /* initialize the list. */
    spin_lock_irq(&ring_mm_element_list_lock);
    INIT_LIST_HEAD(&ring_mm_element_list);
    spin_unlock_irq(&ring_mm_element_list_lock);

    sock_register(&ring_mm_family_ops);
    set_skb_ring_handler(ring_mm_skb_handler);
    set_buffer_ring_handler(ring_mm_buffer_handler);

    if(unlikely(get_buffer_ring_handler() != ring_mm_buffer_handler)) {
	printk(KERN_ERR "PF_RING: initialization failed.\n");
	set_skb_ring_handler(NULL);
	set_buffer_ring_handler(NULL);
	sock_unregister(PF_RING);
	return -1;
    }

    ring_mm_proc_init();

#if defined(RING_MM_SAMPLING)
    printk(KERN_DEBUG "PF_RING: sample rate      %d [1=no sampling]\n",
	   sample_rate);
#endif
#if defined(RING_MM_TRANSMIT)
    printk(KERN_DEBUG "PF_RING: capture TX       %s\n",
	   enable_tx_capture ? "Yes [RX+TX]" : "No [RX only]");
#endif
#if defined(RING_MM_TRANSPARENT)
    printk(KERN_DEBUG "PF_RING: transparent mode %s\n",
	   transparent_mode ? "Yes" : "No");
#endif

    printk(KERN_INFO "PF_RING (memory mapped) initialization complete.\n");
    return 0;
}
module_init(ring_init);



/**************************************
 * ring_mm_element_list_rcu_cleanup()
 *
 * Dispose of the ring_mm_element that has been unlinked
 * after the required RCU grace period.
 */
static void ring_mm_element_list_rcu_cleanup( struct rcu_head *rcu ) {
    struct ring_mm_element *entry = 
	container_of(rcu, struct ring_mm_element, rcu);
    kfree(entry);
}



/**************************************
 * ring_exit()
 *
 * Called when the driver is unloaded, handles cleanup.
 */
static void __exit ring_exit(void)
{
    struct ring_mm_element	*ptr;

    set_skb_ring_handler(NULL);
    set_buffer_ring_handler(NULL);
    sock_unregister(PF_RING);
    ring_mm_proc_term();

    rcu_read_lock();
    spin_lock_irq(&ring_mm_element_list_lock);
    list_for_each_entry_rcu(ptr, &ring_mm_element_list, list) {
	list_del_rcu(&ptr->list);
	call_rcu(&ptr->rcu, ring_mm_element_list_rcu_cleanup);
    }
    spin_unlock_irq(&ring_mm_element_list_lock);
    rcu_read_unlock();

    printk(KERN_INFO "PF_RING unloaded.\n");
}
module_exit(ring_exit);



/**************************************
 * ring_mm_setsockopt()
 *
 * Code taken/inspired from core/sock.c
 *
 * Allows for various options to be changed at runtime.
 */
static int ring_mm_setsockopt(struct socket *sock, int level, int optname,
			      char *optval, int optlen) {

    struct ring_mm_state *state =
	(struct ring_mm_state *) sock->sk->sk_protinfo;
    int retval = 0;

    if(state == NULL) {
	printk(KERN_DEBUG "PF_RING: ring_mm_setsockopt() - top EINVAL\n");
	return(-EINVAL);
    }

#if 0
    if (get_user(val, (int *)optval))
	return -EFAULT;
#endif

    switch(optname) {
    case RING_MM_FLIP:
#if defined(RING_MM_DEBUG) && 0
	printk(KERN_DEBUG "PF_RING: ring_mm_setsockopt() - RING_MM_FLIP "
	       "%p, %d\n", optval, optlen);
#endif
	/*
	 * NOTE: we disallow an optval because we overload the optlen to 
	 * pass the information we need.
	 */
	if ( optval != NULL ) {
#if defined(RING_MM_DEBUG)
	    printk(KERN_DEBUG "PF_RING: ring_mm_setsockopt() - RING_MM_FLIP "
		   "optval not null, returning -EINVAL\n");
#endif
	    return -EINVAL;
	}
#if defined(RING_MM_DEBUG)
	if(spin_is_locked(&state->lock)) {
	    printk("IS LOCKED -- at RING_MM_FLIP");
	}
#endif
	spin_lock_bh(&state->lock);
	{
	    struct ring_mm_data_header *header;
	    header = (struct ring_mm_data_header *)state->memory_address;

	    /* update the state to note where the  */
	    state->read_offset = optlen;

	    /*
	     * if the buffer is empty right now, start filling at
	     * the beginning.
	     */
	    if ( state->read_offset == state->write_offset ) {
		state->read_offset  = 0;
		state->write_offset = 0;
	    }

	    /* update the shared memory header */
	    header->magic_a             = 0xdeadbeef;
	    header->read_offset         = state->read_offset;
	    header->write_offset        = state->write_offset;
	    header->wrap_offset         = state->wrap_offset;
	    header->capture_count       = state->capture_count;
	    header->drop_count          = state->drop_count;
	    header->total_capture_count = state->total_capture_count;
	    header->total_drop_count    = state->total_drop_count;
	    header->magic_b             = 0xcafebabe;

	    retval = state->capture_count;

	    /* update the state for the next read. */
	    state->drop_count    = 0;
	    state->capture_count = 0;

#if defined(RING_MM_DEBUG)
	    if ( ( state->read_offset != 0 ) && ( state->write_offset != 0 )) {
		printk(KERN_DEBUG "PF_RING: %s header read=%u, write=%u, "
		       "wrap=%u, captured=%u/%llu, dropped=%u/%llu\n",
		       state->netdev->name, header->read_offset,
		       header->write_offset, header->wrap_offset,
		       header->capture_count, header->total_capture_count,
		       header->drop_count, header->total_drop_count);
	    }
#endif

	}
	spin_unlock_bh(&state->lock);
	return retval;

    case SO_ATTACH_FILTER:
	retval = -EINVAL;
	if (optlen == sizeof(struct sock_fprog)) {
	    unsigned int fsize;
	    struct sock_fprog fprog;
	    struct sk_filter *filter;
	    
	    retval = -EFAULT;
	    /*
	     * NOTE: Do not call copy_from_user() can sleep and should not
	     * be called with a held spinlock (e.g. state->lock).
	     */
	    if(copy_from_user(&fprog, optval, sizeof(fprog))) {
		break;
	    }

	    fsize = sizeof(struct sock_filter) * fprog.len;
	    filter = kmalloc(fsize, GFP_KERNEL);

	    if(filter == NULL) {
		retval = -ENOMEM;
		break;
	    }

	    if(copy_from_user(filter->insns, fprog.filter, fsize))
		break;

	    filter->len = fprog.len;
	    if(sk_chk_filter(filter->insns, filter->len) != 0) {
		/* Bad filter specified */
		kfree(filter);
		state->bpf_filter = NULL;
		break;
	    }

	    /* get the lock, set the filter, release the lock */
	    spin_lock_bh(&state->lock);
	    state->bpf_filter = filter;
	    spin_unlock_bh(&state->lock);
	}
	retval = 0;
	break;

    case SO_DETACH_FILTER:
	spin_lock_bh(&state->lock);
	if(state->bpf_filter != NULL) {
	    kfree(state->bpf_filter);
	    state->bpf_filter = NULL;
	    spin_unlock_bh(&state->lock);
	    break;
	}
	retval = -ENONET;
	spin_unlock_bh(&state->lock);
	break;

#if 0
    case SO_SET_REFLECTOR:
	if(optlen >= (sizeof(devName)-1))
	    return -EINVAL;

	if(optlen > 0) {
	    if(copy_from_user(devName, optval, optlen))
		return -EFAULT;
	}

	devName[optlen] = '\0';
#if defined(RING_MM_DEBUG)
	printk(KERN_DEBUG "+++ SO_SET_REFLECTOR(%s)\n", devName);
#endif

	spin_lock_bh(&state->lock);
	state->reflector_dev = dev_get_by_name(devName);
	spin_unlock_bh(&state->lock);

#if defined(RING_MM_DEBUG)
	if(state->reflector_dev != NULL) {
	    printk(KERN_DEBUG "SO_SET_REFLECTOR(%s): succeded\n", devName);
	} else {
	    printk(KERN_DEBUG "SO_SET_REFLECTOR(%s): device unknown\n",
		   devName);
	}
#endif
	break;
#endif

    case RING_MM_SNAPLEN:
	/*
	 * NOTE: we disallow an optval because we overload the optlen to 
	 * pass the information we need.
	 */
	if ( optval != NULL ) {
	    return -EINVAL;
	}
#if defined(RING_MM_DEBUG)
	if(spin_is_locked(&state->lock)) {
	    printk("IS LOCKED -- at RING_MM_SNAPLEN");
	}
#endif
	spin_lock_bh(&state->lock);
	state->snaplen = optlen;
	spin_unlock_bh(&state->lock);
	return state->snaplen;

#ifdef CONFIG_PACKET_MMAP
    case PACKET_RX_RING:
        {
	    struct ring_mm_req request;

	    if (optlen < sizeof(request)) {
		printk("PF_RING: PACKET_RX_RING, Invalid request\n");
		return -EINVAL;
	    }
	    if (copy_from_user(&request, optval, sizeof(request))) {
		printk("PF_RING: PACKET_RX_RING, EFAULT\n");
		return -EFAULT;
	    }
	    return ring_mm_set_buffer(sock->sk, &request, 0);
        }
#endif

    default:
	return(sock_setsockopt(sock, level, optname, optval, optlen));
    }

    return(retval);
}



/*****************************************
 * ring_mm_ioctl()
 *
 * I/O Control routines, pass most of them thru.
 */
static int ring_mm_ioctl(struct socket *sock, unsigned int cmd,
			 unsigned long arg) {
    switch(cmd)	{
#ifdef CONFIG_INET
    case SIOCGIFFLAGS:
    case SIOCSIFFLAGS:
    case SIOCGIFCONF:
    case SIOCGIFMETRIC:
    case SIOCSIFMETRIC:
    case SIOCGIFMEM:
    case SIOCSIFMEM:
    case SIOCGIFMTU:
    case SIOCSIFMTU:
    case SIOCSIFLINK:
    case SIOCGIFHWADDR:
    case SIOCSIFHWADDR:
    case SIOCSIFMAP:
    case SIOCGIFMAP:
    case SIOCSIFSLAVE:
    case SIOCGIFSLAVE:
    case SIOCGIFINDEX:
    case SIOCGIFNAME:
    case SIOCGIFCOUNT:
    case SIOCSIFHWBROADCAST:
	return(inet_dgram_ops.ioctl(sock, cmd, arg));
#endif

    default:
	return -ENOIOCTLCMD;
    }

    return 0;
}



/**************************************
 * ring_mm_create()
 *
 * Called when a packet capture session is started.
 */
static int ring_mm_create(struct socket *sock, int protocol) {
    struct sock *sk;
    struct ring_mm_state *state;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_create()\n");
#endif

    /* Are you root, superuser or so ? */
    if(!capable(CAP_NET_ADMIN))
	return -EPERM;

    if(sock->type != SOCK_RAW)
	return -ESOCKTNOSUPPORT;

    if(protocol != htons(ETH_P_ALL))
	return -EPROTONOSUPPORT;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(2,6,11))
    sk = sk_alloc(PF_RING, GFP_ATOMIC, &ring_mm_proto, 1);
#else
    sk = sk_alloc(PF_RING, GFP_KERNEL, 1, NULL);
#endif

    if (sk == NULL) {
	return -ENOMEM;
    }

    sock->ops = &ring_mm_ops;
    sock_init_data(sock, sk);
#if (LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,11))
    sk_set_owner(sk, THIS_MODULE);
#endif

    state = kmalloc(sizeof(struct ring_mm_state), GFP_KERNEL);
    if ( !state ) {
	sk_free(sk);
	return -ENOMEM;
    }
    memset(state, 0, sizeof(struct ring_mm_state));
    spin_lock_init(&state->lock);
#if defined(RING_MM_SUPPORT_POLL)
    init_waitqueue_head(&state->ring_slots_waitqueue);
    atomic_set(&state->num_ring_slots_waiters, 0);
#endif
    sk->sk_protinfo     = state;
    sk->sk_family       = PF_RING;
    sk->sk_destruct     = ring_mm_destruct;

    ring_mm_store_sk_entry(sk);
    ring_mm_proc_add( sk->sk_protinfo );

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_create() - created\n");
#endif

    return(0);
}



/**************************************
 * ring_mm_destruct()
 *
 * Called when a packet capture socket is destroyed.
 */
static void ring_mm_destruct(struct sock *sk) {

    struct ring_mm_state *state;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_destruct()\n");
#endif

    state                 = (struct ring_mm_state *)sk->sk_protinfo;

    skb_queue_purge(&sk->sk_receive_queue);

    if (!sock_flag(sk, SOCK_DEAD)) {
#if defined(RING_DEBUG)
	printk(KERN_ERR "PF_RING: attempt to destroy live ring "
	       "socket: %p\n", sk);
#endif
	return;
    }

    BUG_TRAP(!atomic_read(&sk->sk_rmem_alloc));
    BUG_TRAP(!atomic_read(&sk->sk_wmem_alloc));

    kfree(state);
}





/************************************
 * ring_mm_release()
 *
 * Called when a packet capture session is ended.
 */
static int ring_mm_release(struct socket *sock) {

    struct sock 	 *sk;
    struct ring_mm_state *state;
    void 		 *mapped_address;
    int			  index;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: called ring_mm_release()\n");
#endif

    if(!sock) {
	printk(KERN_DEBUG "PF_RING: ring_mm_release() - null sock\n");
	return 0;
    }
    sk = sock->sk;

    if(!sk) {
	printk(KERN_DEBUG "PF_RING: ring_mm_release() - null sk\n");
	return 0;
    }
    state = (struct ring_mm_state *) sk->sk_protinfo;


    /*
     * The calls below must be placed outside the
     * spin_lock_irq...spin_unlock_irq block.
     */
    sock_orphan(sk);
    ring_mm_proc_remove( (struct ring_mm_state *)sk->sk_protinfo );

    ring_mm_remove_sk_entry(sk);

    spin_lock_bh(&state->lock);
    sock->sk = NULL;
    /* dis-associate the memory for the ringbuffers */
    mapped_address = (void *) state->mapped_address;
    state->mapped_address = (unsigned long) NULL;
    state->memory_address = (unsigned long) NULL;
    sk->sk_protinfo = NULL;
    spin_unlock_bh(&state->lock);

    // Release the state memory allocted in ring_mm_create
    skb_queue_purge(&sk->sk_write_queue);

    sock_put(sk);

    /* the actual unmapping must be done outside the lock.. */
#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_release() - calling vunmap(%p)\n",
	   mapped_address);
#endif

    for ( index = 0; index < state->page_count; index++ ) {
	ClearPageReserved(state->raw_pages[index]);
    }
    vunmap(mapped_address);
    kfree(state->raw_pages);

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_release leaving\n");
#endif

    return 0;
}



/************************************
 * ring_mm_bind()
 *
 * Bind to a device.
 */
static int ring_mm_bind(struct socket *sock, struct sockaddr *sa, int addr_len) {
    struct sock *sk = sock->sk;
    struct ring_mm_state *state = NULL;
    struct net_device *dev = NULL;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_bind() called\n");
#endif

    /* Check legality */
    if (addr_len != sizeof(struct sockaddr)) {
	return -EINVAL;
    }
    if (sa->sa_family != PF_RING) {
	return -EINVAL;
    }

    /* Safety check: add trailing zero if missing */
    sa->sa_data[sizeof(sa->sa_data)-1] = '\0';

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: searching for device %s\n", sa->sa_data);
#endif

    dev = __dev_get_by_name(sa->sa_data);
    if( dev == NULL) {
#if defined(RING_MM_DEBUG)
	printk(KERN_DEBUG "PF_RING: search failed\n");
#endif
	return(-EINVAL);
    }

    state = (struct ring_mm_state *) sk->sk_protinfo;

#if defined(RING_MM_SAMPLING)
    state->pkt_sample_rate = sample_rate;
#endif

    /* set netdev when the socket is ready to be used. */
    state->netdev = dev;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_bind() done for %s\n", 
	   state->netdev->name);
#endif
    return(0);
}



/*****************************************
 * ring_mm_recvmsg()
 *
 * Called when the user requests a packet from our buffer.  This is where
 * we will flip the buffers, etc.  That is what we do when we 'read'.
 */
static int ring_mm_recvmsg(struct kiocb *iocb, struct socket *sock,
			   struct msghdr *msg, size_t len, int flags) {
#if 0
    struct ring_mm_state *state = 
	(struct ring_mm_state *) sock->sk->sk_protinfo;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_recvmsg() called\n");
#endif

    // TODO: implement.  Get lock, swap active buffer, return the number
    //       of bytes that were stored in the buffer that was filled so that
    //       the userland program knows how much data is valid.

    return(queued_pkts);
#endif
    return 0;
}



#if defined(RING_MM_SUPPORT_POLL)
/*****************************************
 * ring_mm_poll()
 *
 * Called when userland tries to read out data.
 */
unsigned int ring_mm_poll(struct file * file,
			  struct socket *sock, poll_table *wait) {

    struct ring_mm_state *state = 
	(struct ring_mm_state *) sock->sk->sk_protinfo;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: poll called\n");
#endif

    slot = get_remove_slot(state);

    if((slot != NULL) && (slot->slot_state == 0)) {
	poll_wait(file, &state->ring_slots_waitqueue, wait);
    }

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: poll returning %d\n", slot->slot_state);
#endif

    if((slot == NULL) || (slot->slot_state != 1)) {
	return(0);
    }

    return(POLLIN | POLLRDNORM);
}
#endif


#if defined(RING_MM_SUPPORT_MMAP)
/*****************************************
 * ring_mm_mmap_open()
 */
static void ring_mm_mmap_open(struct vm_area_struct *vma)
{
    struct file *file = vma->vm_file;
    struct socket * sock = file->private_data;
    struct sock *sk = sock->sk;

    if (sk)
	atomic_inc(&pkt_sk(sk)->mapped);
}


/*****************************************
 * ring_mm_mmap_close()
 */
static void ring_mm_mmap_close(struct vm_area_struct *vma)
{
    struct file *file = vma->vm_file;
    struct socket * sock = file->private_data;
    struct sock *sk = sock->sk;

    if (sk)
	atomic_dec(&pkt_sk(sk)->mapped);
}
#endif


/*****************************************
 * ring_mm_set_buffer()
 *
 * Called to prepare the memory buffer used to store packets.
 */
static int ring_mm_set_buffer(struct sock *sk,
			      struct ring_mm_req *request, int closing) {

    struct ring_mm_state *state = (struct ring_mm_state *)sk->sk_protinfo;
    void *mapped_address = NULL;
    unsigned long offset;
    unsigned long base_address;
    int page_request;
    int index;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_set_buffer()\n");
#endif

    if (unlikely(request->memory_address == 0)) {
	printk(KERN_INFO "PF_RING: invalid memory address for buffer.\n");
	return -EINVAL;
    }

    if (unlikely(request->memory_size < RING_MM_BUFFER_MIN_SIZE)) {
	printk(KERN_INFO "PF_RING: invalid memory size: %d\n",
	       request->memory_size);
	return -EINVAL;
    }

    offset = ((unsigned long) request->memory_address) % PAGE_SIZE;
    base_address = ((unsigned long) request->memory_address) - offset;

    /* how many pages did the user allocate to be mapped? */
    page_request = request->memory_size / PAGE_SIZE;
    state->raw_pages = kmalloc(page_request *sizeof(struct page*), GFP_KERNEL);
    
 retry:
    down_read(&current->mm->mmap_sem);
    state->page_count = get_user_pages(current, current->mm, base_address,
				       page_request, 1, 0, state->raw_pages,
				       NULL);
    up_read(&current->mm->mmap_sem);

    if (state->page_count == -ENOMEM && is_init(current)) {
	congestion_wait(WRITE, HZ/50);
	goto retry;
    }

    if ( state->page_count <= 0 ) {
	printk(KERN_ERR "PF_RING: unable to get user pages: %d\n",
	       state->page_count);
	return state->page_count;
    }

    /* store the results of the request into our state object */
    mapped_address = vmap(state->raw_pages, state->page_count,
			  VM_MAP, PAGE_KERNEL);
    if ( mapped_address == (unsigned long) NULL ) {
	printk(KERN_ERR "PF_RING: unable to map userspace buffer\n");
	return -1;
    }

#if defined(RING_MM_DEBUG)
    if(spin_is_locked(&state->lock)) {
	printk("IS LOCKED -- at ring_mm_set_buffer");
    }
#endif
    spin_lock_bh(&state->lock);
    state->mapped_address  = (unsigned long) mapped_address;
    state->memory_size     = request->memory_size - offset;
    state->memory_address  = state->mapped_address + offset;

    state->data_address = ( state->memory_address + 
			    sizeof(struct ring_mm_data_header) );
    state->data_size    = ( ( state->page_count * PAGE_SIZE ) -
			    sizeof(struct ring_mm_data_header) - offset );
    spin_unlock_bh(&state->lock);

    for ( index = 0; index < state->page_count; index++ ) {
	SetPageReserved(state->raw_pages[index]);
    }

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: buffer vmap'd at 0x%p (0x%p-0x%p) "
	   "of size 0x%x (%d) - %d pages.\n",
	   (void *) state->mapped_address, 
	   (void *) state->memory_address,
	   (void *) (state->memory_address + state->memory_size ),
	   state->memory_size, state->memory_size,
	   state->page_count );

    printk(KERN_DEBUG "PF_RING: ring_mm_data_header is 0x%x (%d) bytes\n", 
	   sizeof(struct ring_mm_data_header),
	   sizeof(struct ring_mm_data_header) );

    printk(KERN_DEBUG "PF_RING: data area is 0x%p-0x%p of size 0x%x (%d)\n",
	   (void *) state->data_address,
	   (void *) (state->data_address + state->data_size),
	   state->data_size, state->data_size );

    if ( state->data_address - state->memory_address != 
	 sizeof(struct ring_mm_data_header ) ) {
	printk(KERN_DEBUG "PF_RING: start points misalligned: "
	       "0x%p + 0x%d != 0x%p\n",
	       (void *) state->memory_address,
	       sizeof(struct ring_mm_data_header),
	       (void *) state->data_address );
    }

    if ( state->data_address + state->data_size !=
	 state->memory_address + state->memory_size ) {
	printk(KERN_DEBUG "PF_RING: areas have different endpoints: "
	       "0x%p (0x%p, 0x%x) != 0x%p (0x%p, 0x%x)\n",
	       (void *) (state->memory_address + state->memory_size),
	       (void *) state->memory_address, state->memory_size,
	       (void *) (state->data_address + state->data_size),
	       (void *) state->data_address, state->data_size );
    }
#endif

    return state->memory_size;
}


/*****************************************
 * ring_mm_buffer_handler()
 *
 * Called to add a packet to the ring buffer... only sometimes used.
 */
static int ring_mm_buffer_handler(struct net_device *dev,
				  char *data, int len) {

    struct list_head	*ptr;
    struct sk_buff	 skb;

#if defined(RING_MM_DEBUG)
    printk(KERN_DEBUG "PF_RING: ring_mm_buffer_hander() called\n");
#endif

    /* Store the data into an sk_buff */
    skb.dev          = dev;
    skb.len          = len;
    skb.data         = data;
    skb.data_len     = len;

    /* Calculate the time */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,14))
    do_gettimeofday(&skb.stamp);
#else
    __net_timestamp(&skb);
#endif

    rcu_read_lock();
    for ( ptr = ring_mm_element_list.next;
	  ptr != &ring_mm_element_list;
	  ptr = ptr->next ) {
	struct ring_mm_state *state;
	struct ring_mm_element *entry;
	
	entry = list_entry( ptr, struct ring_mm_element, list);
	state = (struct ring_mm_state *) entry->sk->sk_protinfo;

	if ( state != NULL ) { 
	    if  ( state->netdev == skb.dev || 
		  ( skb.dev->flags & IFF_SLAVE && 
		    state->netdev == skb.dev->master ) ) {
		
		/* We've found a ring where the packet should be stored */
		add_skb_to_ring(&skb, state, 1, 0);
	    }
	}
    }
    rcu_read_unlock();
    return(0);
}



/*****************************************
 * ring_mm_skb_handler()
 *
 * Called to add a packet to the ring buffer.
 *
 * NOTE: recv_packet indicates if it is 'trasmitted' or 'received'
 *       real_skb indicates if the skb is real or faked. 
 */
static int ring_mm_skb_handler(struct sk_buff *skb, u_char recv_packet,
			       u_char real_skb ) {

    struct list_head	*ptr;
    int			 retval = 0;

    if ( !skb ) {
	/* invalid skb */
#if defined(RING_MM_DEBUG_PACKETS)
	printk(KERN_DEBUG "PF_RING: ring_mm_skb_hander() - invalid skb\n");
#endif
	return 0;
    }

#if defined(RING_MM_TRANSMIT)
    if (!enable_tx_capture && !recv_packet) {
	/* transmitted packet that we are not interested in */
#if defined(RING_MM_DEBUG_PACKETS)
	printk(KERN_DEBUG "PF_RING: ring_mm_skb_hander() - transmitted\n");
#endif
	return 0;
    }
#else
    if ( !recv_packet ) {
	/* transmitted packet that we are not interested in */
#if defined(RING_MM_DEBUG_PACKETS)
	printk(KERN_DEBUG "PF_RING: ring_mm_skb_hander() - transmitted\n");
#endif
	return 0;
    }
#endif

    rcu_read_lock();
    for ( ptr = ring_mm_element_list.next;
	  ptr != &ring_mm_element_list;
	  ptr = ptr->next ) {
	struct ring_mm_state *state;
	struct ring_mm_element *entry;
	
	entry = list_entry( ptr, struct ring_mm_element, list);
	state = (struct ring_mm_state *) entry->sk->sk_protinfo;

	if ( state != NULL ) { 
	    if  ( state->netdev == skb->dev || 
		  ( skb->dev->flags & IFF_SLAVE && 
		    state->netdev == skb->dev->master ) ) {
		
		/* We've found a ring where the packet should be stored */
		add_skb_to_ring(skb, state, recv_packet, real_skb);
		retval = 1;
	    }
	} else {
#if defined(RING_MM_DEBUG_PACKETS)
	    printk(KERN_DEBUG "PF_RING: ring_mm_skb_hander() - null state.\n");
#endif
	}
    }
    rcu_read_unlock();

#if defined(RING_MM_TRANSPARENT)
    if( transparent_mode ) {
	/* pretend we didn't handle it. */
	retval = 0;
    }
#endif

    /* if we've handled it and it's a real sk_buff, free it. */
    if ( retval && real_skb ) {
	dev_kfree_skb(skb);
    }
    
    return retval;
}

#define COUNTER_LIMIT 30
static void add_skb_to_ring(struct sk_buff *skb, struct ring_mm_state *state,
			    unsigned char recv_pkt, unsigned char real_skb) {

    int remaining_space = 0;
    struct pcap_pkthdr *pcap;
    void *data_out;
    int snaplen = MAX_PACKET_VIS_SIZE;
    int displacement;
    int captured_size;
    int wrapped;
    int needed_space;


#if defined(RING_MM_DEBUG_PACKETS)
    static int counter = 0;
    int full_printed = FALSE;

    printk(KERN_DEBUG "PF_RING: add_skb_to_ring(%p, %p, %d, %d) "
	   "- %s, %p\n", skb, state, recv_pkt, real_skb,
	   (state->netdev != NULL ? state->netdev->name : "null" ),
	   (void *) state->memory_address );
#endif

    
    /* can't do anything if we have no memory yet. */
    if ( ! state->memory_address ) {
	return;
    }
    
    /* Hack for identifying a packet received by the e1000 */
    if(recv_pkt) {
	if(real_skb) {
	    displacement = SKB_DISPLACEMENT;
	} else
	    /* Received by the e1000 wrapper */
	    displacement = 0;
    } else {
	displacement = 0;
    }

    captured_size = skb->len + displacement;
    needed_space = PCAP_HEADER_SIZE + captured_size;

#if defined(RING_MM_DEBUG_PACKETS)
    if ( counter < COUNTER_LIMIT ) {
	printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - %s, write_offset=%d, "
	       "displacement=%d, caplen=%d, pcap_pkthdr=%d, needed_space=%d\n",
	       (state->netdev != NULL ? state->netdev->name : "null" ),
	       state->write_offset, displacement, skb->len,
	       PCAP_HEADER_SIZE, needed_space );
    }
#endif

#if defined(RING_MM_DEBUG)
    if(spin_is_locked(&state->lock)) {
	printk("IS LOCKED -- at add_skb_to_ring");
    }
#endif
    spin_lock_bh(&state->lock);
    if ( state->read_offset > state->write_offset ) {
	wrapped = TRUE;
    } else {
	wrapped = FALSE;
    }

#if defined(RING_MM_DEBUG)
    if ( state->write_offset >= state->data_size ) {
	printk(KERN_ERR "PF_RING: OFF THE END OF THE BUFFER!  %d, %d\n",
	       state->write_offset, state->data_size );
    }
#endif
  
    if ( wrapped ) {
	remaining_space = ( state->read_offset - state->write_offset - 
			    MIN_SPACE_REMAINING );
	if ( needed_space >= remaining_space ) {
	    /* no space left, and we're already wrapped, so we're done */
	    state->drop_count++;
	    state->total_drop_count++;
	    spin_unlock_bh(&state->lock);
#if defined(RING_MM_DEBUG_PACKETS)
	    if ( ! full_printed ) {
		printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
		       "wrapped no space: read=%u, write=%u, "
		       "data_size=%u\n", state->read_offset,
		       state->write_offset, state->data_size );
		full_printed = TRUE;
	    }
#endif
	    return;
	}

    } else {
	remaining_space = ( state->data_size - state->write_offset - 
			    MIN_SPACE_REMAINING );
	if ( needed_space >= remaining_space ) {
	    /* no space at the end of the buffer.. try at the front. */
#if defined(RING_MM_DEBUG_PACKETS)
	    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
		   "no space at end\n" );
#endif
	    remaining_space = state->read_offset - MIN_SPACE_REMAINING;
	    if ( needed_space >= remaining_space ) {
		state->drop_count++;
		state->total_drop_count++;
		spin_unlock_bh(&state->lock);
#if defined(RING_MM_DEBUG_PACKETS)
		if ( ! full_printed ) {
		    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
			   "un-wrapped no space: read=%u, write=%u, "
			   "data_size=%u\n", state->read_offset,
			   state->write_offset, state->data_size );
		    full_printed = TRUE;
		}
#endif
		return;
	    }

	    /*
	     * at this point there is space at the front,
	     * reset the pointer to use it.
	     */
#if defined(RING_MM_DEBUG_PACKETS)
	    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
		   "wrapping to start\n" );
#endif
	    state->wrap_offset = state->write_offset;
	    state->write_offset = 0;
	}
    }
    /* at this point, we have found a space large enough to hold the packet. */
#if defined(RING_MM_DEBUG_PACKETS)
    full_printed = FALSE;
#endif

    /* check to see if any filter applies, and if it matches. */
    if ( state->bpf_filter != NULL ) {
	skb->data -= displacement;
	snaplen = sk_run_filter( skb, state->bpf_filter->insns,
				 state->bpf_filter->len );
	skb->data += displacement;

	if ( snaplen == 0 ) {
	    /* not a drop.. but we don't want to store the packet. */
	    spin_unlock_bh(&state->lock);
#if defined(RING_MM_DEBUG_PACKETS)
	    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
		   "did not pass filter.\n" );
#endif
	    return;
	}
    }

    /* we want the smaller of the filter's snaplen or the ring's snaplen. */
    snaplen = MIN( snaplen, state->snaplen );

#if defined(RING_MM_SAMPLING)
    if(state->pkt_sample_rate > 1) {
	if(state->pkt_sample_count != 0) {
	    state->pkt_sample_count--;
	    spin_unlock_bh(&state->lock);
	    return;
#if defined(RING_MM_DEBUG_PACKETS)
	    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - "
		   "discard due to sample_rate.\n" );
#endif
	}
	state->pkt_sample_count = state->pkt_sample_rate;
    }
#endif

#if defined(RING_MM_DEBUG_PACKETS)
    printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - storing at 0x%p, "
	   "pcap@%d, pcap_size=%d, needed_space=%d, \n", 
	   (void *) ( state->data_address + state->write_offset ),
	   state->write_offset, PCAP_HEADER_SIZE, needed_space );
#endif

    /* Store the pcap header information in the buffer. */
    pcap = (struct pcap_pkthdr *) (state->data_address + state->write_offset);

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,14))
    if ( ! skb->stamp.tv_sec ) {
	do_gettimeofday(skb.stamp);
    }
    pcap->ts.tv_sec  = skb->stamp.tv_sec;
    pcap->ts.tv_usec = skb->stamp.tv_usec;
#else
    if ( ! skb->tstamp.off_sec ) {
	__net_timestamp(skb);
    }
    pcap->ts.tv_sec  = skb->tstamp.off_sec;
    pcap->ts.tv_usec = skb->tstamp.off_usec;
#endif
    pcap->caplen    = skb->len + displacement;

    if(pcap->caplen > state->snaplen)
	pcap->caplen = state->snaplen;

    pcap->len = skb->len + displacement;

    /* Store the packet itself. */
    data_out = (void *) ( state->data_address + state->write_offset + 
			  PCAP_HEADER_SIZE );

    memcpy( data_out, skb->data - displacement, pcap->caplen );

    /* update the write offset to point to the next spot to put data. */
    state->write_offset += PCAP_HEADER_SIZE + pcap->caplen;

    state->capture_count++;
    state->total_capture_count++;

    spin_unlock_bh(&state->lock);

#if defined(RING_MM_DEBUG_PACKETS)
    if ( counter < COUNTER_LIMIT ) {
	printk(KERN_DEBUG "PF_RING: add_skb_to_ring() - %s, "
	       "new write_offset=%d -- wrote: %lu/%lu, %u/%u\n",
	       (state->netdev != NULL ? state->netdev->name : "null" ),
	       state->write_offset, pcap->ts.tv_sec, pcap->ts.tv_usec,
	       pcap->caplen, pcap->len );
	counter++;
    }
#endif
}






/**************************************
 * ring_mm_store_sk_entry()
 *
 * Called to store the sock structure so we can search for it
 * when a packet arrives and needs to be stored.
 */
static inline void ring_mm_store_sk_entry(struct sock *sk) {
    struct ring_mm_element *element;

#if defined(RING_DEBUG)
    prink(KERN_DEBUG "PF_RING: ring_mm_store_state()\n");
#endif

    element = kmalloc( sizeof(struct ring_mm_element), GFP_ATOMIC);
    if ( element == NULL ) {
	if ( net_ratelimit() ) {
	    printk(KERN_ERR "PF_RING: could not allocate sock "
		   "ring_mm_element\n");
	}
	return;
    }

    element->sk = sk;
    
    rcu_read_lock();
    spin_lock_irq(&ring_mm_element_list_lock);
    list_add_tail_rcu(&element->list, &ring_mm_element_list);
    spin_unlock_irq(&ring_mm_element_list_lock);
    rcu_read_unlock();
}



/**************************************
 * ring_mm_remove_sk_entry()
 *
 * Called when a sock structure needs to be removed from the list,
 * eg. when the session is ending.
 */
static inline void ring_mm_remove_sk_entry(struct sock *sk) {

    struct list_head *ptr;
    struct ring_mm_element *entry = NULL;

    rcu_read_lock();
    spin_lock_irq(&ring_mm_element_list_lock);
    for( ptr = ring_mm_element_list.next;
	 ptr != &ring_mm_element_list;
	 ptr = ptr->next ) {
	entry = list_entry(ptr, struct ring_mm_element, list);
	if(entry->sk == sk) {
	    list_del_rcu(ptr);
	    break;
	}
    }
    spin_unlock_irq(&ring_mm_element_list_lock);
    rcu_read_unlock();

    /* schedule the element we removed for deletion later, when it's safe. */
    call_rcu(&entry->rcu, ring_mm_element_list_rcu_cleanup);
}



/**************************************
 * ring_mm_proc_init()
 */
static void ring_mm_proc_init(void) {
    ring_mm_proc_dir = proc_mkdir("pf_ring", proc_net);

    if(!ring_mm_proc_dir) {
	printk(KERN_ERR "PF_RING: unable to create /proc/net/pf_ring\n");
	return;
    }

    ring_mm_proc_dir->owner = THIS_MODULE;
    ring_mm_proc = create_proc_read_entry("info", 0, ring_mm_proc_dir,
					  ring_mm_proc_get_info, NULL);
    if(!ring_mm_proc) {
	printk(KERN_ERR "PF_RING: unable to register proc file\n");
	return;
    }

    ring_mm_proc->owner = THIS_MODULE;
#if defined(DEBUG)
    printk(KERN_DEBUG "PF_RING: registered /proc/net/pf_ring/\n");
#endif
}



/**************************************
 * ring_mm_proc_term()
 */
static void ring_mm_proc_term(void) {
    if(ring_mm_proc != NULL) {
	remove_proc_entry("info", ring_mm_proc_dir);
	if(ring_mm_proc_dir != NULL) {
	    remove_proc_entry("pf_ring", proc_net);
	}
#if defined(DEBUG)
	printk(KERN_DEBUG "PF_RING: deregistered /proc/net/pf_ring\n");
#endif
    }
}



/**************************************
 * ring_mm_proc_add()
 */
static void ring_mm_proc_add(struct ring_mm_state *state) {
    if(ring_mm_proc_dir != NULL) {
	char name[16];

	state->ring_id = ring_count++;

	snprintf(name, sizeof(name), "%d", state->ring_id);
	create_proc_read_entry(name, 0, ring_mm_proc_dir,
			       ring_mm_proc_get_info, state);
#if defined(DEBUG)
	printk(KERN_DEBUG "PF_RING: added /proc/net/pf_ring/%s\n", name);
#endif
    }
}



/**************************************
 * ring_mm_proc_remove()
 */
static void ring_mm_proc_remove(struct ring_mm_state *state) {
    if(ring_mm_proc_dir != NULL) {
	char name[16];

	snprintf(name, sizeof(name), "%d", state->ring_id);
	remove_proc_entry(name, ring_mm_proc_dir);
#if defined(DEBUG)
	printk(KERN_DEBUG "PF_RING: removed /proc/net/pf_ring/%s\n", name);
#endif
    }
}



/**************************************
 * ring_mm_proc_get_info()
 */
static int ring_mm_proc_get_info(char *buf, char **start, off_t offset,
				 int len, int *unused, void *data) {
    int rlen = 0;
    struct ring_mm_state *state;

    if(data == NULL) {
	/* /proc/net/pf_ring/info */
	rlen = sprintf(buf,        "Version       : %s (mm)\n", RING_VERSION);
#if defined(RING_MM_SAMPLING)
	rlen += sprintf(buf + rlen,"Sample rate   : %d [1=no sampling]\n",
			sample_rate);
#endif
#if defined(RING_MM_TRANSMIT)
	rlen += sprintf(buf + rlen,"Capture TX    : %s\n",
			enable_tx_capture ? "Yes [RX+TX]" : "No [RX only]");
#endif
    } else {
	/* detailed statistics about a PF_RING */
	state = (struct ring_mm_state *)data;

	rlen = sprintf(buf,        "Bound Device  : %s\n",
		       state->netdev->name == NULL ?
		       "<NULL>" : state->netdev->name);
	rlen += sprintf(buf + rlen,"Version       : %s\n",
			RING_VERSION);
#if defined(RING_MM_SAMPLING)
	rlen += sprintf(buf + rlen,"Sampling Rate : %d\n",
			state->sample_rate);
#endif
	rlen += sprintf(buf + rlen,"Memory mapped : %d\n",
			state->memory_size);
	rlen += sprintf(buf + rlen,"Total Packets : %llu\n",
			state->total_capture_count);
	rlen += sprintf(buf + rlen,"Drop events   : %llu\n",
			state->total_drop_count);
	rlen += sprintf(buf + rlen,"Current pkts  : %ud\n",
			state->capture_count);
	rlen += sprintf(buf + rlen,"Current drops : %ud\n",
			state->drop_count);

    }

    return rlen;
}
