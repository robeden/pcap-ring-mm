/*
 * Copyright (C) 2007, NEXVU Technoligies
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef __cplusplus 
extern "C" {
#endif

/***************************************
 * Defines
 */
#if !defined(FALSE)
# define	FALSE		0
#endif

#if !defined(TRUE)
# define	TRUE		1
#endif

#define RING_MM_VERSION		"0.10"

#define PF_RING			27
#define SOCK_RING		PF_RING

#define RING_MM_FLIP		0xfe000001
#define RING_MM_SNAPLEN		0xfe000002

#define PFRING_MM_NAME_SIZE	32

#if defined(__x86_64__)
# define PCAP_HEADER_SIZE	24
#else
# define PCAP_HEADER_SIZE	16
#endif



/***************************************
 * Structure definitions
 */
struct ring_mm_data_header {
    unsigned int        magic_a;
    unsigned int        read_offset;
    unsigned int        write_offset;
    unsigned int        wrap_offset;
    unsigned int        reserved;
    unsigned int	capture_count;
    unsigned int	drop_count;
    unsigned long long	total_capture_count;
    unsigned long long	total_drop_count;
    unsigned int        magic_b;
};


struct ring_mm_req {
    unsigned long        memory_address;
    unsigned int         memory_size;
};


struct pfring_mm_state {
    char 		 name[PFRING_MM_NAME_SIZE];
    int			 sock_fd;
    int			 clear_promisc;
    int			 ring_size;
    void 		*ring_memory;	
};


#if !defined(lib_pcap_h) && 0
struct pcap_pkthdr {
    struct timeval	 ts;      /* time stamp */
    unsigned int	 caplen;  /* length of portion present */
    unsigned int	 len;     /* length this packet (off wire) */
};
#endif


/***************************************
 * Prototypes
 */
struct pfring_mm_state *pfring_mm_open(char *name, int buffer_size,
				       int promisc, int snaplen);
void pfring_mm_close(struct pfring_mm_state *state);
int pfring_mm_set_promisc(struct pfring_mm_state *state, int promisc);

void *pfring_mm_get_ring_address(struct pfring_mm_state *state);
int pfring_mm_get_data_capacity(struct pfring_mm_state *state);

int pfring_mm_read(struct pfring_mm_state *state, int read_offset);
int pfring_mm_get_read_offset(struct pfring_mm_state *state);
int pfring_mm_get_write_offset(struct pfring_mm_state *state);
int pfring_mm_get_wrap_offset(struct pfring_mm_state *state);
int pfring_mm_get_drop_count(struct pfring_mm_state *state);

#ifdef __cplusplus
}
#endif
